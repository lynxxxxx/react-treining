import { ProfileContext } from "../hoc/ProfileContext";
import { useContext, useEffect } from 'react';

const ProfileCard = () => {

    const [profile, setProfile] = useContext(ProfileContext)

    useEffect(() => async () => {
        fetch('https://randomuser.me/api/')
      .then(response => response.json())
      .then(_json => _json.results[0])
      .then(data => setProfile({
        username: data.login.username,
        picture: data.picture.thumbnail
      }))
          
        .catch(e => console.log(e))
        
       }, [setProfile])
     

    const handleLogout = () =>{
        setProfile(null)
    }
    return ( 
        <>
        {profile &&
        <div className="ProfileCard">
            <div className="image1"><img src={profile.picture} alt="profile" /></div>
            <div className="item2"><span>{profile.username}</span></div>
            <div className="item3"><button onClick={handleLogout}>Logout</button></div>
         
        </div>
        }
        </>
     );
}
 
export default ProfileCard;