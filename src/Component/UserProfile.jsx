import React, {useEffect, useState} from 'react';
import UserProfileCard from './UserprofileCard';


const UserProfile = () => {
   
    const [user, setUser] = useState({})
    const [data, setData] = useState({})

     useEffect(() => {
      const fetchUser = async () => {
        try{
     const response = await fetch('https://randomuser.me/api/')
     const data = await response.json()
     const dataResult = data.results[0]
     const userData = {
     username: dataResult.login.username,
     country: dataResult.location.country,
     name: `${dataResult.name.first}`,
     picture: dataResult.picture.thumbnail
     }
     console.log(data)
     setData(userData)
        }
        catch (error) {
    console.log(error.message)
        }
      }
      fetchUser()
     }, [user])
   //handler
    const handleOnClick = (event) => {
        setUser({username: "rinat"})
    }
    

    return ( 
        <>
        <div>User data</div>
        { user.username ? <p>{user.username}</p> : <p>No exist</p> }
        <button onClick={handleOnClick}>set user</button>
        {/* <img src={data.picture} alt="Profile" />
        <p>{data.username}</p>
        <p>{data.country}</p>
        <p>{data.name}</p> */}
        <UserProfileCard userdata = {data}/>
        </>
     );
}
 
export default UserProfile;