import { useContext } from 'react';
import './App.css';
import ProfileCard from './Component/ProfileCard';
import SimpleForm from './Component/SimpleForm';
import { ProfileContext } from './hoc/ProfileContext';
import {BrowserRouter, Routes, Route, NavLink } from 'react-router-dom';
import ProfilePage from './pages/ProfilePage';
import keycloak from './keycloak/keycloak';
import LoggedInRoute from './routes/LoggedInRoute';
import TokenPage from './Component/TokenPage';

function App() {

  const [,setProfile] = useContext(ProfileContext)

 

  return (
    <BrowserRouter>
     <div className="App">
     <h1>My demo app</h1>
  
    <Routes>
    <Route path="/"  element={<p>Home page</p>}/>
      <Route path="/profile" element={<ProfilePage />}/>
      <Route path="/form"  element={
        <LoggedInRoute>
            <SimpleForm />
        </LoggedInRoute>
    
      }/>
       <Route path="/token"  element={
        <LoggedInRoute>
            <TokenPage />
        </LoggedInRoute>
    
      }/>
    </Routes>
    <footer>
      <h4>Navigate </h4>
      <nav>
       <li><NavLink to="/profile">Go to profile</NavLink> </li> 
       <li><NavLink to="/form">Go to form</NavLink></li> 
       <li><NavLink to="/token">Go to Token</NavLink></li> 
       { keycloak.authenticated 
       ? <li><button onClick={() => {keycloak.logout()}}>logout from keycloak</button></li> 
       :  <li><button onClick={() => {keycloak.login()}}>login to keycloak</button></li> 
       }
      
       
      </nav>
    </footer>
    </div>
    </BrowserRouter>
   
    
  );
}

export default App;
