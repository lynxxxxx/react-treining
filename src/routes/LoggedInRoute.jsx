import {  NavLink } from "react-router-dom";
import keycloak from "../keycloak/keycloak";

function LoggedInRoute({ children }) {

  
  if (keycloak.authenticated) {
    return(
        <>{children}</>
    )
  } 

   else {
    return(
        <>
        <p>you must be logged in to view thi page</p>
         <NavLink  to='/' >Return to home</NavLink>;
        </>
    )
  }

 
}

export default LoggedInRoute;
