import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import ProfileProvider from './hoc/ProfileContext';
import { initialize } from './keycloak/keycloak';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
<p>Please wait while keycloak is loading</p>
)
initialize().then(() => {
  root.render(
    <React.StrictMode>
      <ProfileProvider>
      <App />
      </ProfileProvider>
     
    </React.StrictMode>
  );
  
}).catch(() => {
  root.render(
    <React.StrictMode>
      <h1>Keycloak is not loading something is filed😒</h1>
    </React.StrictMode>
    
  )
})


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
